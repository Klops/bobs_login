<div class="users form">
    <?= $this->Flash->render() ?>
  <div class="login-container animated fadeInDown">

<div class="row">
  <div class="col-lg-4"></div>
  <div class="col-lg-4 content">
    <h2>Login</h2>
    <?= $this->Form->create() ?>
    <fieldset>
      <h3><?= __('Please enter your username and password') ?></h3>
      <hr>
      <?= $this->Form->control('email',
        [ 'class' => 'form-control',
          'required' => true]) ?>
      <?= $this->Form->control('password',
        [ 'class' => 'form-control',
          'required' => true]) ?>
    </fieldset>
    <hr>
    <?= $this->Form->submit(__('Login')); ?>
    <?= $this->Form->end() ?>
    </div>
  <div class="col-lg-4"></div>
  </div>

    <?= $this->Html->link("Register", ['action' => 'add']) ?>
</div>