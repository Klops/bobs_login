<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="users index content">

    <h1>Thank you for taking the time to see me! </h1>

    <h4>I do hope that this quick build has been enough to convince you of my skills!</h4>

    <p>I hope to hear from you soon!</p>

  <?= $this->Html->link(__('Logout'), ['action' => 'logout'], ['class' => 'button']) ?>

</div>
