<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
    <div class="col-lg-4"></div>
    <div class="col-lg-4">
<!--    <div class="column-responsive column-80">-->
        <div class="users form content">
            <?= $this->Form->create($user) ?>
            <fieldset>
                <legend><?= __('Add User') ?></legend>
                <?php
                    echo $this->Form->control('email');
                    echo $this->Form->control('name');
                    echo $this->Form->control('surname');
                    echo $this->Form->control('password');
                ?>
            </fieldset>
            <hr>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
            <hr>
          <?= $this->Html->link("Return to Login", ['action' => 'login']) ?>
        </div>
<!--    </div>-->
    </div>
    <div class="col-lg-4"></div>
</div>
